PyPI Release Checklist
======================

For Every Release
-------------------

#. Update HISTORY.rst

#. Update version number (can also be patch or major)

    .. code-block:: bash

        bump2version minor

#. Run the static analysis and tests:

    .. code-block:: bash

        tox

#. Commit the changes:

    .. code-block:: bash

        git add HISTORY.rst
        git commit -m "Changelog for upcoming release <#.#.#>"

#. Push the commit:

    .. code-block:: bash

        git push

#. Add the release tag on GitLab: https://gitlab.com/genomicsengland/opensource/templates/cookiecutter-pyproject/-/tags

About This Checklist
--------------------

This checklist is adapted from:

* https://gist.github.com/audreyr/5990987
* https://gist.github.com/audreyr/9f1564ea049c14f682f4

It assumes that you are using all features of Cookiecutter PyPackage.
