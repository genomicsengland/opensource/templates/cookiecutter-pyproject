# {{ cookiecutter.project_name }}

{{ cookiecutter.project_short_description }}

## Features

* TODO

## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter) and the [opensource/templates/cookiecutter-pypackage](https://gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage) project template.
