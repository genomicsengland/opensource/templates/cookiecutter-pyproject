# Config file for GitLab CI pipeline

stages:
  - build
  - compliance
  - test
  - build_docs
  - publish_docs
  - publish_pypi

default:
  tags:
    # See https://cnfl.extge.co.uk/display/BPS/Using+CE+hosted+gitlab+runner for correct tag to use
    - pool_name:interpretation_services_test

# Configuration --------------------------------------------------------------------

variables:
  BUILD_PYTHON_VERSION: "3.10"

.docker_in_docker:
  services:
    - name: docker:24.0.5-dind
      command:
        [
          "--registry-mirror",
          "https://docker.artifactory.aws.gel.ac",
        ]
  before_script: |-
    apk add --no-cache make git
    docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    docker context create {{ cookiecutter.project_slug }}
    docker buildx create --name=buildkit-builder --driver=docker-container --use {{ cookiecutter.project_slug }}

.test_docker_image:
  image: ${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}:py${python_version}-${CI_COMMIT_SHORT_SHA}

.python_version_matrix:
  parallel:
    matrix:
      - python_version: [ "3.10", "3.11.1", "3.12" ]

.default_rules:
  rules:
    # Run on {{ cookiecutter.trunk_branch }} branch
    - if: $CI_COMMIT_BRANCH == "{{ cookiecutter.trunk_branch }}"
      when: on_success
    # Run on appropriate tags
    - if: $CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+.*/
      when: on_success
    # Run on patch branches
    - if: $CI_COMMIT_BRANCH != "{{ cookiecutter.trunk_branch }}" && $CI_COMMIT_BRANCH !~ "/^patch-/"
      when: on_success
    # Everything else don't run
    - when: never

.run_on_{{ cookiecutter.trunk_branch }}:
  rules:
    # Run on {{ cookiecutter.trunk_branch }} branch
    - if: $CI_COMMIT_BRANCH == "{{ cookiecutter.trunk_branch }}"
      when: on_success
    # Everything else don't run
    - when: never

.run_on_release_tag:
  rules:
    # Only publish with suitable tag
    - if: $CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+.*/
      when: on_success
    # Everything else don't run
    - when: never

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_COMMIT_TAG

# Jobs: Build -----------------------------------------------------------------

build:
  stage: build
  extends:
    - .docker_in_docker
    - .default_rules
    - .python_version_matrix
  script:
    - PYTHON_VERSION=${python_version} PYPI_URL=${pip_repository_index} make build
    - PYTHON_VERSION=${python_version} PYPI_URL=${pip_repository_index} make push

# Jobs: Compliance ------------------------------------------------------------

compliance:
  stage: compliance
  extends:
    - .python_version_matrix
    - .test_docker_image
  script:
    - poetry run invoke format --check
    - poetry run invoke lint
    - poetry run invoke security

# Jobs: Test ------------------------------------------------------------------

test:
  stage: test
  extends:
    - .python_version_matrix
    - .test_docker_image
  script:
    - poetry run invoke test --junit --coverage
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: ${CI_PROJECT_DIR}/bin/coverage.xml
      junit: ${CI_PROJECT_DIR}/bin/report.xml

# Jobs: Docs -----------------------------------------------------------------------

build_docs:
  variables:
    python_version: $BUILD_PYTHON_VERSION
  stage: build_docs
  extends:
    - .test_docker_image
  script:
    - poetry run invoke docs --no-launch
  artifacts:
    paths:
      - docs/_build

pages:
  stage: publish_docs
  extends:
    - .run_on_{{ cookiecutter.trunk_branch }}
  dependencies:
    - build_docs
  script:
    - mv docs/_build public/
  artifacts:
    paths:
      - public

# Jobs: Publish ---------------------------------------------------------------

release_pypi:
  variables:
    python_version: $BUILD_PYTHON_VERSION
  stage: publish_pypi
  script:
{%- if cookiecutter.build_backend == 'poetry' %}
    - |
      poetry run invoke release-poetry \
        --pypi-user=${pypi_user} \
        --pypi-pass=${pypi_pass} \
        --pypi-publish-repository=${pypi_publish_repository}
{%- endif %}{%- if cookiecutter.build_backend == 'setuptools' %}
    - |
      poetry run invoke release-twine \
        --tag-name="${tag_name}" \
        --pypi-user="${pypi_user}" \
        --pypi-pass="${pypi_pass}" \
        --pypi-publish-repository="${pypi_publish_repository}" \
        --pip-repository-index="${pip_repository_index}"
{%- endif %}
  extends:
    - .run_on_release_tag
    - .test_docker_image
