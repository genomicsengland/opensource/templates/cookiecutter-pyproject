"""Console script for {{cookiecutter.project_slug}}."""

import argparse


def main() -> None:
    """Console script for {{cookiecutter.project_slug}}."""
    parser = argparse.ArgumentParser()
    parser.add_argument("_", nargs="*")
    args = parser.parse_args()

    print("Arguments: " + str(args._))
    print("Replace this message by putting your code into {{cookiecutter.project_slug}}.cli.main")


if __name__ == "__main__":
    main()
