# Development

## Get Started!

Ready to contribute? Here\'s how to set up {{ cookiecutter.project_slug }} for local development.

1.  Clone the {{ cookiecutter.project_slug }} repo from GitLab:

    ``` shell
    $ git clone git@gitlab.com:genomicsengland/cancer_analysis/{{ cookiecutter.project_slug }}.git
    ```

2.  Ensure [poetry is installed](https://python-poetry.org/docs/).

3.  Install dependencies and start your virtualenv:

    ``` shell
    $ poetry install
    $ poetry shell
    ```

4.  Create a branch for local development:

    ``` shell
    $ git checkout -b Jira-ticket-ID/name-of-your-bugfix-or-feature
    ```

Now you can make your changes locally.

1.  When you\'re done making changes, check that your changes pass the
    tests, including testing other Python versions, with invoke:

    ``` shell
    $ invoke test --coverage
    ```

2.  Commit your changes and push your branch to GitLab:

    ``` shell
    $ git add .
    $ git commit -m "Your detailed description of your changes."
    $ git push origin Jira-ticket-ID/name-of-your-bugfix-or-feature
    ```

3.  Submit a merge request through GitLab

## Merge Request Guidelines

Before you submit a merge request, check that it meets these guidelines:

1.  The merge request should only include changes relating to one
    ticket.
2.  The merge request should include tests to cover any added changes
    and check that all existing and new tests pass.
3.  If the merge request adds functionality, the docs should be updated.
    Put your new functionality into a function with a docstring, and add
    the feature to the list in README.rst.
4.  The team should be informed of any impactful changes.

## Tips

1.  To run a subset of tests:

    ``` shell
    $ pytest tests.test_{{ cookiecutter.project_slug }}
    ```

## Deploying to PyPI

For every release:

1.  Update CHANGELOG.md

    ``` shell
    invoke changelog
    ```

2.  Update version number (can also be patch or major):

    ``` shell
    bump2version minor
    ```

3.  Run the pre-commit hooks (static analysis and tests):

    ``` shell
    pre-commit run --all-files
    ```

4.  Commit the changes:

    ``` shell
    git commit -am "[Jira-ticket-ID] Changelog for upcoming release <#.#.#>"
    ```

5.  Push the commit:

    ``` shell
    git push
    ```

#. Add the release tag (version) on GitLab: https://gitlab.com/{{ cookiecutter.project_gitlab_path }}/-/tags

The GitLab CI pipeline will then deploy if tests pass.
