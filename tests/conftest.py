"""
Config for pytest
"""


def pytest_addoption(parser):
    """
    Add options to control pytest
    """
    parser.addoption(
        "--run-slow",
        action="store_true",
        default=False,
        help="Run slow tests",
    )
