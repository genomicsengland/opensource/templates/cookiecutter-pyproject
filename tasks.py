"""Development tasks for the cookiecutter template project"""
import inspect
import shutil
import webbrowser
from pathlib import Path
import platform

import pytest
from invoke import task, exceptions  # type: ignore

# For python 3.11 support
if not hasattr(inspect, "getargspec"):
    inspect.getargspec = inspect.getfullargspec

ROOT_DIR = Path(__file__).parent
DOCS_DIR = ROOT_DIR.joinpath("docs")
DOCS_BUILD_DIR = DOCS_DIR.joinpath("_build")
DOCS_INDEX = DOCS_BUILD_DIR.joinpath("index.html")
TOX_DIR = ROOT_DIR.joinpath(".tox")
TEST_CACHE_DIR = ROOT_DIR.joinpath(".pytest_cache")
POETRY_REGISTRY_URL = "registry.gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage/poetry:latest"
PYPI_URL = "https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple"
PYTHON_VERSION = "3.10"
DOCKERFILE = "Dockerfile"
DOCKER_PLATFORM = "--platform linux/amd64"


def _run(_c, command):
    return _c.run(command, pty=platform.system() != "Windows")


@task(
    optional=["python_version"],
    help={
        "python_version": 'Python version to use, e.g. "3.9"',
    },
)
def docker_build(_c, python_version=PYTHON_VERSION):
    """
    Build poetry Docker container

    :param _c: The context object that is passed to invoke tasks
    :param python_version: The base python version to use
    """
    build_args = (
        f"--build-arg PYTHON_BASE={python_version} --build-arg PYPI_URL={PYPI_URL}"
    )
    _run(
        _c,
        f"docker build {build_args} {DOCKER_PLATFORM} -f {DOCKERFILE} -t {POETRY_REGISTRY_URL} .",
    )


@task
def docker_pull(_c):
    """
    It pulls the image from the local registry, or if it doesn't exist, it prints a message

    :param _c: The context object that is passed to invoke tasks
    """
    _run(_c, f'docker pull {POETRY_REGISTRY_URL} || echo "No pre-made image available"')


@task
def docker_push(_c):
    """
    Push poetry Docker container to registry

    :param _c: The context object that is passed to invoke tasks
    """
    _run(_c, f"docker push {POETRY_REGISTRY_URL}")


@task
def docker_shell(_c):
    """
    It opens shell in the docker container

    :param _c: The context object that is passed to invoke tasks
    """
    volume_mount = f"--volume {ROOT_DIR}:/cookiecutter-pypackage:rw"
    shell_path = "/bin/bash"
    _run(
        _c,
        f"docker run -it {DOCKER_PLATFORM} {volume_mount} {POETRY_REGISTRY_URL} {shell_path}",
    )


@task
def test(_, run_slow=False):
    """
    Run tests

    :param run_slow: Run slow tests (default False)
    """
    pytest_args = ["-v"]
    if run_slow:
        pytest_args.append("-s")
        pytest_args.append("--run-slow")

    return_code = pytest.main(pytest_args)
    if return_code:
        raise exceptions.Exit("Tests failed", code=return_code)


@task
def clean_docs(_):
    """
    Clean up files from documentation builds
    """
    shutil.rmtree(DOCS_BUILD_DIR, ignore_errors=True)


@task
def clean_tests(_):
    """Clean up files from testing"""
    shutil.rmtree(TEST_CACHE_DIR, ignore_errors=True)


@task
def clean_tox(_):
    """Clean up files from testing"""
    shutil.rmtree(TOX_DIR, ignore_errors=True)


@task(pre=[clean_docs, clean_tests, clean_tox])
def clean(_):
    """Runs all clean sub-tasks"""


@task(pre=[clean_docs], help={"launch": "Launch documentation in the web browser"})
def docs(_c, launch=True):
    """
    Generate documentation

    :param _c: The context object that is passed to invoke tasks
    :param launch: Launch docs in browser (default True)
    """
    _run(_c, f"sphinx-build -b html {DOCS_DIR} {DOCS_BUILD_DIR}")
    if launch:
        webbrowser.open(DOCS_INDEX.as_uri())
