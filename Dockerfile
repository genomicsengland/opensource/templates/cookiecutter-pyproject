# Dockerfile to build base Docker image
# Example to build:
# $ docker build --build-arg PYTHON_BASE=3.10 --build-arg PYPI_URL=https://artifactory.aws.gel.ac/artifactory/api/pypi/pypi/simple --platform linux/amd64 -f Dockerfile -t registry.gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage/poetry:latest .
# -or-
# $ inv docker-build
# Example to push:
# $ docker push registry.gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage/poetry:latest
# -or-
# $ inv docker-push`
ARG PYTHON_BASE

FROM docker.artifactory.aws.gel.ac/python:${PYTHON_BASE}-slim-bullseye as base
ARG PYPI_URL

WORKDIR /src

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=0 \
    PIP_INDEX_URL=$PYPI_URL

COPY pyproject.toml .
COPY poetry.lock .

RUN sed -i 's|http://|https://artifactory.aws.gel.ac/artifactory/apt_|g' /etc/apt/sources.list

RUN apt-get update -y \
    && apt-get install -y --no-install-recommends \
    git \
    && pip install -Iv --prefer-binary --index-url ${PYPI_URL} --upgrade \
    pip \
    poetry \
    && poetry install --no-root
